import subprocess, os, string, time
import xml.etree.ElementTree as ET
from fontTools.pens.cocoaPen import CocoaPen
from robofab.interface.all.dialogs import GetFolder, GetFile, AskString
from lib.UI.spaceCenter.glyphSequenceEditText import splitText
from vanilla import *
from drawBot import *
from drawBot.ui.drawView import DrawView 
        
            
class CommitArrayWindow(object):
    def __init__(self, inputFont, since, until):
        self.inputFont = inputFont
        self.since = since
        self.until = until
        self.w = FloatingWindow((200, 200), "Select Commit Array")
        self.commitsList = self.gitCommitArray(inputFont, self.since, self.until, comments=True)
        self.commitsList.reverse()
        self.commitsHashList = self.gitCommitArray(inputFont, self.since, self.until)
        self.commitsHashList.reverse()
        self.w.startTextBox = TextBox((10, 10, -10, 17), "Start From:")
        self.w.startPopUpButton = PopUpButton((10, 30, -10, 20),
                              self.commitsList,
                              callback=self.startPopUpButtonCallback)
        self.w.startPopUpButton.set(0)
        self.start = 0
        self.w.stopTextBox = TextBox((10, 50, -10, 17), "Stop At:")
        self.w.stopPopUpButton = PopUpButton((10, 70, -10, 20),
                              self.commitsList,
                              callback=self.stopPopUpButtonCallback)
        self.w.stopPopUpButton.set(len(self.commitsList)-1)
        self.stop = len(self.commitsList)-1
        self.w.numberOfSelectedCommits = TextBox((10, 100, -10, 17), str(self.stop-self.start)+" Commits Selected")
        self.w.individualGlyphsCheckBox = CheckBox((10, 120, -10, 20), 'One Movie per Glyph')
        self.w.goButton = Button((10, 140, -10, 20), "Make Movie",
                            callback=self.goButtonCallback)
        self.w.bar = ProgressBar((10, 170, -10, 16))
        self.w.open()
    
    def startPopUpButtonCallback(self, sender):
        self.start = sender.get()
        self.w.numberOfSelectedCommits.set( str(self.stop-self.start)+" Commits Selected" )
    
    def stopPopUpButtonCallback(self, sender):
        self.stop = sender.get()
        self.w.numberOfSelectedCommits.set( str(self.stop-self.start)+" Commits Selected" )
    
    def goButtonCallback(self, sender):
        self.commitsHashList = self.commitsHashList[self.start:self.stop]
        self.makeMovie((1000, 1000), self.commitsHashList, text=None, individualGlyph=True) 
        self.w.close()
    
    def gitCommitArray(self, inputFont, sinceDate, untilDate, comments=False):
        inputFolder, font = os.path.split(inputFont)
        since = None
        until = None
        if sinceDate != None:
            since = '--since=' + sinceDate
        if untilDate != None:
            until = '--until=' + untilDate
            
        if comments == True:
            pretty = '--pretty=format: %s %h %ad'
        else:
            pretty = '--pretty=%h'
            
        if since == None and until == None:
            commits = subprocess.check_output(['git', 'log', pretty, inputFont], cwd=inputFolder)
        elif since != None and until == None:
            commits = subprocess.check_output(['git', 'log', pretty, since, inputFont], cwd=inputFolder)
        elif since == None and until != None:
            commits = subprocess.check_output(['git', 'log', pretty, until, inputFont], cwd=inputFolder)
        elif since != None and until != None:
            commits = subprocess.check_output(['git', 'log', pretty, since, until, inputFont], cwd=inputFolder)
            
        commitsList = commits.split('\n')
        return commitsList
    
    def makeMovie(self, (width, height), commitList, text=None, individualGlyph=False):   
        outputPath = GetFolder("Select an Output Folder for .mov")
        repoBase = self.getRepoBase(inputFont)
        start = time.time()
        individualGlyph = self.w.individualGlyphsCheckBox.get()
        if not individualGlyph:
            self.dbw = Window((width, height))
            self.dbw.drawBotCanvas = DrawView((0, 0, -0, -0))
            self.dbw.open()
            self.genTempImages((width, height), repoBase, inputFont, text, commitList)
            endGen = time.time()
            moviePath = os.path.join(outputPath, 'movie.mov')
            endMov = time.time()
            saveImage([moviePath])
            print 'generating %s images in %s minutes' % ( str(len(commitList)), str(int((endGen - start)/60)) )
            print 'generating movie in %s seconds' % str((int(endMov - endGen)))
        else:
            glyph2Commits = self.makeGlyph2Commits(repoBase, commitList)
            for g, commits in glyph2Commits.iteritems():
                if len(commits) <= 1: continue
                moviePath = os.path.join(outputPath, g+'.mov')
                newDrawing()
                self.w.bar.set(0)
                inc = 100.0/len(commits)
                dbw = Window((width, height))
                dbw.drawBotCanvas = DrawView((0, 0, -0, -0))
                dbw.open()
                for commit in commits:
                    self.gitCheckoutOld(commit, repoBase)
                    self.genTempImage((width, height), inputFont, [g])
                    self.w.bar.increment(inc)
                pdfData = pdfImage()
                dbw.drawBotCanvas.setPDFDocument(pdfData)
                saveImage([moviePath])
                dbw.close()
            subprocess.call(['git', 'stash'], cwd=repoBase)
            subprocess.call(['git', 'checkout', 'master'], cwd=repoBase)


    def makeGlyph2Commits(self, repoBase, commitList):
        glyph2Commits = {}
        #switch to latest commit to get the 'full' glyph set
        self.gitCheckoutOld(commitList[-1], repoBase)
        f = RFont(self.inputFont, showUI=False)
        for g in f:
            glyph2Commits[g.name] = []
        f.close()
        self.w.bar.set(0)
        inc = 100.0/len(commitList)
        for commitHash in commitList:
            self.gitCheckoutOld(commitHash, repoBase)
            log = subprocess.check_output(['git', 'log', '-1', '--pretty=format:', '--name-only'], cwd=repoBase)
            f = RFont(self.inputFont, showUI=False)
            for l in log.split('\n'):
                headG, tailG = os.path.split(l)
                if tailG.split('.')[-1] == 'glif':
                    glifName = self.getGlyphNameformGLIF(repoBase, l)
                    if glifName in f and glifName in glyph2Commits:
                        glyph2Commits[glifName].append(commitHash)
            self.w.bar.increment(inc)
            f.close()
        return glyph2Commits

    def getRepoBase(self, inputFont):
        inputFolder, font = os.path.split(inputFont)
        repoBase = subprocess.check_output(['git', 'rev-parse', '--show-toplevel'], cwd=inputFolder)
        while repoBase[ -1 ] == '\n':
            repoBase = repoBase[:-1]
        return repoBase
    
    def genTempImages(self, (width, height), repoBase, inputFont, text, commitList):
        head, tail = os.path.split(inputFont)
        commits = commitList
        newDrawing()
        self.w.bar.set(0)
        inc = 100.0/len(commits)
        for commitHash in commits:
            self.gitCheckoutOld(commitHash, repoBase)
            glyphsToDislayList = []
            glyphsEdited = []
            f = RFont(inputFont, showUI=False)
            if text == None:
                log = subprocess.check_output(['git', 'log', '-1', '--pretty=format:', '--name-only'], cwd=repoBase)
                for l in log.split('\n'):
                    headG, tailG = os.path.split(l)
                    if tailG.split('.')[-1] == 'glif':
                        glifName = self.getGlyphNameformGLIF(repoBase, l)
                        if glifName in f:
                            glyphsToDislayList.append(glifName)
            else:
                log = subprocess.check_output(['git', 'log', '-1', '--pretty=format:', '--name-only'], cwd=repoBase)
                for l in log.split('\n'):
                    headG, tailG = os.path.split(l)
                    if tailG.split('.')[-1] == 'glif':
                        glifName = self.getGlyphNameformGLIF(repoBase, l)
                        if glifName in f:
                            glyphsEdited.append(glifName)
                for glifName in splitText(text, f.naked().unicodeData):
                    if glifName in f:
                        glyphsToDislayList.append(glifName)
            if glyphsToDislayList != []:
                skipImage = False
                if glyphsEdited != []:
                    for g in glyphsEdited:
                        if g not in glyphsToDislayList:
                            skipImage = True
                            break
                if skipImage:
                    self.w.bar.increment(inc)
                    continue
                self.genTempImage((width, height), inputFont, glyphsToDislayList)
                self.w.bar.increment(inc)
            f.close()
        pdfData = pdfImage()
        self.dbw.drawBotCanvas.setPDFDocument(pdfData)
        subprocess.call(['git', 'stash'], cwd=repoBase)
        subprocess.call(['git', 'checkout', 'master'], cwd=repoBase)
    
    def gitCheckoutOld(self, commitHash, path):
        subprocess.call(['git', 'stash'], cwd=path)
        subprocess.call(['git', 'checkout', commitHash], cwd=path)
        
    def getGlyphNameformGLIF(self, repoBase, glifFile):
        filePath = os.path.join(repoBase, glifFile)
        try:
            tree = ET.parse(filePath)
            root = tree.getroot()
            return root.get('name')
        except:
            return ''
            
    def genTempImage(self, (docWidth, docHeight), font, glyphNameList):
        newPage(docWidth, docHeight)
        pointSize = docHeight*(60/100)
        f = RFont(font, showUI=False)
        UPM = f.info.unitsPerEm
        desc = f.info.descender
        asc = f.info.ascender
        box_Y = asc - desc 
        pitch = 1.0*pointSize / UPM 
    
        margins = 40
        box_X = 0
        for g in glyphNameList:
            if g not in f:
                f.close()
                return
            box_X += f[g].width
        xPos = (docWidth/pitch)*.5 - box_X*.5
        yPos = (docHeight/pitch)*.5 - (box_Y*pitch)*.5
        scale(pitch)
        for i, g in enumerate(glyphNameList):
            try:
                nextglyph = f[g]
                if i < len(glyphNameList)-1:
                    nextglyph = f[glyphNameList[i+1]]
                glyph = f[g]
                save()
                translate(xPos, yPos)
                self._drawGlyph(glyph)
                restore()
                xPos += glyph.width
            except:
                print '? missing glyph', g
                print f.glyphOrder
        f.close()
                
    def _drawGlyph(self, glyph):
        pen = CocoaPen(glyph.getParent())
        glyph.draw(pen)
        drawPath(pen.path)
        
inputFont = GetFile("Select Input UFO")
sinceDate = AskString("Parse commits Since (DD/MM/YYYY)")
untilDate = AskString("Parse commits Until (DD/MM/YYYY)")
CommitArrayWindow(inputFont, sinceDate, untilDate)